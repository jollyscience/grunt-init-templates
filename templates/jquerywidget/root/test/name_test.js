/*global QUnit:false, module:false, test:false, asyncTest:false, expect:false*/
/*global start:false, stop:false ok:false, equal:false, notEqual:false, deepEqual:false*/
/*global notDeepEqual:false, strictEqual:false, notStrictEqual:false, raises:false*/
(function($) {

  /*
    ======== A Handy Little QUnit Reference ========
    http://docs.jquery.com/QUnit

    Test methods:
      expect(numAssertions)
      stop(increment)
      start(decrement)
    Test assertions:
      ok(value, [message])
      equal(actual, expected, [message])
      notEqual(actual, expected, [message])
      deepEqual(actual, expected, [message])
      notDeepEqual(actual, expected, [message])
      strictEqual(actual, expected, [message])
      notStrictEqual(actual, expected, [message])
      raises(block, [expected], [message])
  */

  module('jQueryWidget', {
    setup: function() {
      this.widget = ($('#qunit-fixture').{%= name %}({
        someValue: 'initialVal'
      }));
    }
  });

  test('has methods', function() {
    ok(this.widget.{%= name %}('methodA'), 'should have a method called methodA');
    ok(this.widget.{%= name %}('methodB'), 'should have a method called methodB');
  });
  
  test('can change options', function () {
    var new_val = 'newVal';
    this.widget.{%= name %}('option', 'someValue', new_val);
    equal(this.widget.{%= name %}('option', 'someValue'), new_val, 'new value should now be set');
  });
  
}(jQuery));
