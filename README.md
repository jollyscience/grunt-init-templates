grunt-init-templates
====================

A collection of Grunt Init Templates.
    
## Install (with global grunt.js)
If grunt is not yet installed globally
    npm install grunt -g    

    $ cd /usr/local/lib/node_modules/grunt/tasks/init/
    $ git clone git@github.com:meshachjackson/grunt-init-templates.git
    $ cp -r grunt-init-templates/templates/ ./

## Install (with local grunt.js)
    $ cd /my/project/dir/
    $ git clone git@github.com:meshachjackson/grunt-init-templates.git
    $ npm install
    $ 


## TODO
* See: _develop branch_ > /boilerplates-to-convert/
* Create as NPM package, which should handle install / update from this repo
    
## Contribute
Stuff you can help with...
* Add to the /boilerplates-to-convert

How to do it...
* Fork this repo
* Make your changes (and TESTS!)
* Make a Pull Request

THANKS!
@meshachjackson - Jolly Science Labs